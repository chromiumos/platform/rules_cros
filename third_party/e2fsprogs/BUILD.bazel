load("@rules_foreign_cc//foreign_cc:defs.bzl", "configure_make")
load("//rules_cros/third_party/rules_foreign_cc:standalone_binary.bzl", "foreign_cc_standalone_binary")

configure_make(
    name = "e2fsprogs",
    configure_options = [
        # Disable all features except fuse2fs, since that's the only one we care
        # about.
        "--disable-debugfs",
        "--disable-imager",
        "--disable-resizer",
        "--disable-degfrag",
        "--disable-fsck",
        "--disable-e2initrd-helper",
        "--disable-rpath",
        "--enable-fuse2fs",

        # By default, these files attempt to output to the host system.
        "--with-udev-rules-dir=../out/lib/udev/rules.d",
        "--with-crond-dir=../out/etc/cron.d",
        "--with-systemd-unit-dir=../out/lib/systemd/system",

        # Optimizations
        "--enable-lto",
    ],
    install_prefix = "out",
    lib_source = "@e2fsprogs//:all_srcs",
    out_binaries = ["fuse2fs"],
    deps = ["//rules_cros/third_party/fuse"],
)

foreign_cc_standalone_binary(
    name = "fuse2fs",
    src = ":e2fsprogs",
)
